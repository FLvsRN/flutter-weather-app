# Weather app - Flutter

Simple weather mobile app.

## Built with 

- Flutter
- Bloc
- metaweather.com API
- Lottie

## Screenshots

### iOS

###### Home screen

<img src="/screenshots/ios/Simulator Screen Shot - iPhone 8 - 2021-06-21 at 14.26.51.png" height="200">

###### Weather screen

<img src="/screenshots/ios/Simulator Screen Shot - iPhone 8 - 2021-06-21 at 14.27.14.png" height="200">

###### Location change screen

<img src="/screenshots/ios/Simulator Screen Shot - iPhone 8 - 2021-06-21 at 14.26.55.png" height="200">
<img src="/screenshots/ios/Simulator Screen Shot - iPhone 8 - 2021-06-21 at 14.27.02.png" height="200">

###### Refresh view

<img src="/screenshots/ios/Simulator Screen Shot - iPhone 8 - 2021-06-21 at 14.27.17.png" height="200">

### Android

###### Home screen

<img src="/screenshots/android/Screenshot_1624277740.png" height="200">

###### Weather screen

<img src="/screenshots/android/Screenshot_1624277795.png" height="200">

###### Location change screen

<img src="/screenshots/android/Screenshot_1624277767.png" height="200">
<img src="/screenshots/android/Screenshot_1624277782.png" height="200">

###### Refresh view

<img src="/screenshots/android/Screenshot_1624277805.png" height="200">

## License
[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).

[![License: CC BY-SA 4.0](https://licensebuttons.net/l/by-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-sa/4.0/)
