import 'src/app.dart';
import 'package:http/http.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/simple_bloc_observer.dart';
import 'package:weather_app/src/services/api/weather_api_provider.dart';
import 'package:weather_app/src/services/repositories/repositories.dart';

void main() {
  Bloc.observer = SimpleBlocObserver();
  final weatherRepository = WeatherRepository(
    weatherApiClient: WeatherApiProvider(httpClient: Client()),
  );

  runApp(WeatherApp(weatherRepository: weatherRepository));
}
