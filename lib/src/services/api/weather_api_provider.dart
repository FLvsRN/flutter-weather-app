import 'package:meta/meta.dart';
import 'package:http/http.dart';
import 'dart:convert';

import 'package:weather_app/src/services/models/models.dart';

class WeatherApiProvider {
  static const baseUrl = 'https://www.metaweather.com';
  final Client httpClient;

  WeatherApiProvider({
    @required this.httpClient,
  }) : assert(httpClient != null);

  Future<int> getLocationId(String city) async {
    final locationUrl = '$baseUrl/api/location/search/?query=$city';
    final locationResponse = await this.httpClient.get(locationUrl);
    if (locationResponse.statusCode != 200) {
      throw Exception('error getting locationId for city');
    }

    final locationJson = jsonDecode(locationResponse.body) as List;

    return (locationJson.first)['woeid'];
  }

  Future<WeatherList> fetchWeather(int locationId) async {
    final weatherUrl = '$baseUrl/api/location/$locationId/';
    final weatherResponse = await this.httpClient.get(weatherUrl);

    if (weatherResponse.statusCode != 200) {
      throw Exception('error getting weather for location');
    }

    final Map<String, dynamic> map = json.decode(weatherResponse.body);
    final String location = map['title'];
    final int id = map['woeid'];
    final List<dynamic> data = map["consolidated_weather"];
    final List<Weather> weekWeather =
        data.map(((e) => Weather.fromJson(e))).toList();

    return WeatherList(id, location, weekWeather, DateTime.now());
  }
}
