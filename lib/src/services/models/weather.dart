import 'package:equatable/equatable.dart';

enum WeatherState {
  snow,
  sleet,
  hail,
  thunderstorm,
  heavyRain,
  lightRain,
  showers,
  heavyCloud,
  lightCloud,
  clear,
  unknown
}

class Weather extends Equatable {
  final WeatherState state;
  final String stateName;
  final DateTime date;
  final double temperature;
  final double minTemperature;
  final double maxTemperature;
  final int humidity;
  final double windSpeed;
  final String windDirection;
  final double airPressure;
  final double visibility;

  const Weather({
    this.state,
    this.stateName,
    this.date,
    this.temperature,
    this.minTemperature,
    this.maxTemperature,
    this.humidity,
    this.windSpeed,
    this.windDirection,
    this.airPressure,
    this.visibility,
  });

  @override
  List<Object> get props => [
        state,
        stateName,
        date,
        temperature,
        minTemperature,
        maxTemperature,
        humidity,
        windSpeed,
        windDirection,
        airPressure,
        visibility,
      ];

  static Weather fromJson(dynamic json) {
    return Weather(
      state: _mapStringToWeatherCondition(json['weather_state_abbr']),
      stateName: json['weather_state_name'],
      date: DateTime.parse(json['applicable_date']),
      temperature: json['the_temp'] as double,
      minTemperature: json['min_temp'] as double,
      maxTemperature: json['max_temp'] as double,
      humidity: json['humidity'] as int,
      windSpeed: json['wind_speed'] as double,
      windDirection: json['wind_direction_compass'],
      airPressure: json['air_pressure'] as double,
      visibility: json['visibility'] as double,
    );
  }

  static WeatherState _mapStringToWeatherCondition(String input) {
    WeatherState state;
    switch (input) {
      case 'sn':
        state = WeatherState.snow;
        break;
      case 'sl':
        state = WeatherState.sleet;
        break;
      case 'h':
        state = WeatherState.hail;
        break;
      case 't':
        state = WeatherState.thunderstorm;
        break;
      case 'hr':
        state = WeatherState.heavyRain;
        break;
      case 'lr':
        state = WeatherState.lightRain;
        break;
      case 's':
        state = WeatherState.showers;
        break;
      case 'hc':
        state = WeatherState.heavyCloud;
        break;
      case 'lc':
        state = WeatherState.lightCloud;
        break;
      case 'c':
        state = WeatherState.clear;
        break;
      default:
        state = WeatherState.unknown;
    }
    return state;
  }
}
