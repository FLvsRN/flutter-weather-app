import 'package:equatable/equatable.dart';
import 'package:weather_app/src/services/models/models.dart';

class WeatherList extends Equatable {
  final int id;
  final String location;
  final List<Weather> weekWeather;
  final DateTime lastUpdated;

  WeatherList(this.id, this.location, this.weekWeather, this.lastUpdated);

  @override
  List<Object> get props => [id, location, weekWeather, lastUpdated];
}
