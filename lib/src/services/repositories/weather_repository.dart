import 'dart:async';

import 'package:meta/meta.dart';

import 'package:weather_app/src/services/api/weather_api_provider.dart';
import 'package:weather_app/src/services/models/models.dart';

class WeatherRepository {
  final WeatherApiProvider weatherApiClient;

  WeatherRepository({@required this.weatherApiClient})
      : assert(weatherApiClient != null);

  Future<WeatherList> getWeather(String city) async {
    final int locationId = await weatherApiClient.getLocationId(city);

    return weatherApiClient.fetchWeather(locationId);
  }
}
