import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';

import 'package:weather_app/src/features/weather/bloc/bloc.dart';
import 'package:weather_app/src/services/models/weather_list.dart';
import 'package:weather_app/src/services/repositories/repositories.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  final WeatherRepository weatherRepository;

  WeatherBloc({@required this.weatherRepository})
      : assert(weatherRepository != null),
        super(WeatherInitial());

  @override
  Stream<WeatherState> mapEventToState(WeatherEvent event) async* {
    if (event is WeatherRequested) {
      yield* _mapWeatherRequestedToState(event);
    } else if (event is WeatherRefreshRequested) {
      yield* _mapWeatherRefreshRequestedToState(event);
    }
  }

  Stream<WeatherState> _mapWeatherRequestedToState(
      WeatherRequested event) async* {
    yield WeatherLoadInProgress();
    try {
      final WeatherList weatherList =
          await weatherRepository.getWeather(event.city);
      yield WeatherLoadSuccess(weatherList: weatherList);
    } catch (_) {
      yield WeatherLoadFailure();
    }
  }

  Stream<WeatherState> _mapWeatherRefreshRequestedToState(
      WeatherRefreshRequested event) async* {
    try {
      final WeatherList weatherList =
          await weatherRepository.getWeather(event.city);
      yield WeatherLoadSuccess(weatherList: weatherList);
    } catch (_) {}
  }
}
