import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:weather_app/src/services/models/models.dart';
import 'package:weather_app/src/services/models/weather_list.dart';

abstract class WeatherState extends Equatable {
  const WeatherState();

  @override
  List<Object> get props => [];
}

class WeatherInitial extends WeatherState {}

class WeatherLoadInProgress extends WeatherState {}

class WeatherLoadSuccess extends WeatherState {
  final WeatherList weatherList;

  const WeatherLoadSuccess({@required this.weatherList})
      : assert(weatherList != null);

  @override
  List<Object> get props => [weatherList];
}

class WeatherLoadFailure extends WeatherState {}
