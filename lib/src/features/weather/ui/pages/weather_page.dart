import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/src/features/weather/bloc/bloc.dart';
import 'package:weather_app/src/features/weather/ui/widgets/widgets.dart';

class WeatherPage extends StatefulWidget {
  WeatherPage({Key key}) : super(key: key);

  @override
  _WeatherPageState createState() => _WeatherPageState();
}

class _WeatherPageState extends State<WeatherPage> {
  Completer<void> _refreshCompleter;

  @override
  void initState() {
    super.initState();
    _refreshCompleter = Completer<void>();
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('ERROR'),
          content: Text('Something went wrong!'),
          actions: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                TextButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    double width50 = MediaQuery.of(context).size.width * 0.5;
    double height = MediaQuery.of(context).size.height;
    EdgeInsets padding = MediaQuery.of(context).padding;
    double newheight = height - padding.top - padding.bottom;
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.bottomLeft,
          end: Alignment.topRight,
          colors: [
            Color(0xffacb6e5),
            Color(0xff86fde8),
          ],
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SafeArea(
          child: Center(
            child: BlocConsumer<WeatherBloc, WeatherState>(
              listener: (context, state) {
                if (state is WeatherLoadSuccess) {
                  _refreshCompleter?.complete();
                  _refreshCompleter = Completer();
                }
              },
              // ignore: missing_return
              builder: (context, state) {
                if (state is WeatherInitial) {
                  return EnterLocationPrompt(width: width50);
                }
                if (state is WeatherLoadInProgress) {
                  return Center(child: CircularProgressIndicator());
                }
                if (state is WeatherLoadSuccess) {
                  final weatherList = state.weatherList;
                  final weather = weatherList.weekWeather[0];
                  final weekWeather = weatherList.weekWeather
                      .getRange(1, weatherList.weekWeather.length)
                      .toList();

                  return RefreshIndicator(
                      onRefresh: () {
                        BlocProvider.of<WeatherBloc>(context).add(
                          WeatherRefreshRequested(city: weatherList.location),
                        );
                        return _refreshCompleter.future;
                      },
                      child: ListView(
                        children: [
                          Container(
                            height: newheight,
                            child: Column(
                              children: [
                                Flexible(
                                  fit: FlexFit.tight,
                                  flex: 5,
                                  child: CurrentWeather(
                                      weatherList: weatherList,
                                      weather: weather,
                                      width: width50),
                                ),
                                Flexible(
                                  flex: 4,
                                  fit: FlexFit.tight,
                                  child: Column(
                                    children: [
                                      WeekForecast(weekWeather: weekWeather),
                                      WeatherDetails(
                                          weekWeather: weekWeather,
                                          weather: weather),
                                    ],
                                  ),
                                ),
                                Divider(
                                  color: Colors.white,
                                  height: 10,
                                  thickness: 1,
                                ),
                                BottomButton()
                              ],
                            ),
                          ),
                        ],
                      ));
                }
                if (state is WeatherLoadFailure) {
                  Future.microtask(() => _showMyDialog());

                  return EnterLocationPrompt(width: width50);
                }
              },
            ),
          ),
        ),
      ),
    );
  }
}
