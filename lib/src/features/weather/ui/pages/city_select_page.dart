import 'package:flutter/material.dart';

class CitySelectPage extends StatefulWidget {
  CitySelectPage({Key key}) : super(key: key);

  @override
  _CitySelectPageState createState() => _CitySelectPageState();
}

class _CitySelectPageState extends State<CitySelectPage> {
  final TextEditingController _textController = TextEditingController();
  bool _btnEnabled = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff48d3ff),
        title: Text('City'),
        leading: BackButton(color: Colors.black),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Form(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.all(20.0),
                    child: Container(
                      height: 40,
                      child: TextFormField(
                        onChanged: (String txt) {
                          if (txt.length > 0) {
                            setState(() {
                              _btnEnabled = true;
                            });
                          } else {
                            setState(() {
                              _btnEnabled = false;
                            });
                          }
                        },
                        controller: _textController,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(15.0),
                          labelText: "Enter city name...",
                          fillColor: Colors.white,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * .7,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        padding: EdgeInsets.all(10.0),
                        primary: Color(0xff9cbaff),
                      ),
                      onPressed: _btnEnabled
                          ? () => {Navigator.pop(context, _textController.text)}
                          : null,
                      child: Text("SEARCH", style: TextStyle(fontSize: 15)),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Text('Flutter',
                      style: TextStyle(fontWeight: FontWeight.w300)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
