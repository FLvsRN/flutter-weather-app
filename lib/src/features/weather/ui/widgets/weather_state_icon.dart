import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:weather_app/src/services/models/models.dart';

class WeatherStateIcon extends StatelessWidget {
  const WeatherStateIcon({
    Key key,
    @required this.weatherState,
    this.fit,
  }) : super(key: key);

  final WeatherState weatherState;
  final BoxFit fit;

  Widget _mapWeatherStateToLottie(WeatherState state) {
    String fileName;

    switch (state) {
      case WeatherState.snow:
        fileName = 'moderate-snow';
        break;
      case WeatherState.sleet:
        fileName = 'sleet';
        break;
      case WeatherState.hail:
        fileName = 'hail';
        break;
      case WeatherState.thunderstorm:
        fileName = 'thunderstorm';
        break;
      case WeatherState.heavyRain:
        fileName = 'heavy-rain';
        break;
      case WeatherState.lightRain:
        fileName = 'light-rain';
        break;
      case WeatherState.showers:
        fileName = 'showers';
        break;
      case WeatherState.heavyCloud:
        fileName = 'overcast';
        break;
      case WeatherState.lightCloud:
        fileName = 'light-cloud';
        break;
      case WeatherState.clear:
        fileName = 'sunny';
        break;
      default:
        return Center(child: Text('N/A'));
        break;
    }

    return Lottie.asset(
      'assets/lottie/$fileName.json',
      fit: fit,
    );
  }

  @override
  Widget build(BuildContext context) => _mapWeatherStateToLottie(weatherState);
}
