import 'package:flutter/material.dart';
import 'package:weather_app/src/services/models/models.dart';

class WeatherDetails extends StatelessWidget {
  const WeatherDetails({
    Key key,
    @required this.weekWeather,
    @required this.weather,
  }) : super(key: key);

  final List<Weather> weekWeather;
  final Weather weather;

  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: 2,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 5.0),
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          itemCount: weekWeather.length,
          itemBuilder: (context, index) {
            final details = [
              {
                'key': 'Air pressure',
                'value': weather.airPressure.round(),
                'unit': ' hPa'
              },
              {'key': 'Humidity', 'value': weather.humidity, 'unit': '%'},
              {
                'key': 'Wind',
                'value': weather.windSpeed.round(),
                'unit': ' mph'
              },
              {
                'key': 'Wind direction',
                'value': weather.windDirection,
                'unit': ''
              },
              {
                'key': 'Visibility',
                'value': weather.visibility.round(),
                'unit': ' miles'
              },
            ];
            final detail = details[index];
            return FittedBox(
              fit: BoxFit.contain,
              child: Container(
                margin: EdgeInsets.all(5.0),
                padding: EdgeInsets.all(10.0),
                width: 155.0,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5.0)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.all(5.0),
                      child: Text(
                        detail['key'],
                        style: TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.w300),
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.all(5.0),
                        child: Text(
                          '${detail['value']}${detail['unit']}',
                          style: TextStyle(
                              fontSize: 28.0, fontWeight: FontWeight.w400),
                        )),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
