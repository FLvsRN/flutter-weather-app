import 'package:flutter/material.dart';
import 'package:weather_app/src/services/models/models.dart';
import 'package:weather_app/src/features/weather/ui/widgets/widgets.dart';

class CurrentWeather extends StatelessWidget {
  const CurrentWeather({
    Key key,
    @required this.weatherList,
    @required this.weather,
    @required this.width,
  }) : super(key: key);

  final WeatherList weatherList;
  final Weather weather;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          margin: EdgeInsets.all(5.0),
          child: Text(
            weatherList.location,
            style: TextStyle(fontSize: 42.0, fontWeight: FontWeight.w300),
          ),
        ),
        Container(
          margin: EdgeInsets.zero,
          child: Text(weather.stateName,
              style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w200)),
        ),
        Container(
          margin: EdgeInsets.all(15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: WeatherStateIcon(weatherState: weather.state),
              ),
              Expanded(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.all(10.0),
                      padding: EdgeInsets.symmetric(horizontal: 5.0),
                      child: Text('${weather.temperature.round().toString()}℃',
                          style: TextStyle(
                              fontSize: 52.0, fontWeight: FontWeight.w600)),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text(
                            'L:${weather.minTemperature.round().toString()}',
                            style: TextStyle(fontWeight: FontWeight.w300),
                          ),
                          Text(
                            'H:${weather.maxTemperature.round().toString()}',
                            style: TextStyle(fontWeight: FontWeight.w300),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
