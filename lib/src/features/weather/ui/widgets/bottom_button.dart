import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/src/features/weather/bloc/bloc.dart';
import 'package:weather_app/src/features/weather/ui/pages/pages.dart';

class BottomButton extends StatelessWidget {
  const BottomButton({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerRight,
      child: IconButton(
        color: Colors.white,
        icon: Icon(Icons.menu),
        onPressed: () async {
          final city = await Navigator.push(context,
              MaterialPageRoute(builder: (context) => CitySelectPage()));
          if (city != null) {
            BlocProvider.of<WeatherBloc>(context)
                .add(WeatherRequested(city: city));
          }
        },
      ),
    );
  }
}
