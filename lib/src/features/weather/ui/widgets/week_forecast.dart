import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:weather_app/src/services/models/models.dart';
import 'package:weather_app/src/features/weather/ui/widgets/widgets.dart';

class WeekForecast extends StatelessWidget {
  const WeekForecast({
    Key key,
    @required this.weekWeather,
  }) : super(key: key);

  final List<Weather> weekWeather;

  @override
  Widget build(BuildContext context) {
    return Flexible(
      flex: 4, // todo 3 było
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 5.0),
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          shrinkWrap: true,
          itemCount: weekWeather.length,
          itemBuilder: (context, index) {
            final day = weekWeather[index];
            return AspectRatio(
              aspectRatio: 5 / 8,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 15.0),
                padding: EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                  color: Color.fromRGBO(255, 255, 255, 0.5),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        index == 0
                            ? 'Tomorrow'
                            : DateFormat('EEEE').format(day.date),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Container(
                      width: 65,
                      height: 65,
                      child: WeatherStateIcon(
                          weatherState: day.state, fit: BoxFit.scaleDown),
                    ),
                    Text(
                      '${day.temperature.round()}℃',
                      style: TextStyle(fontWeight: FontWeight.w400),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
