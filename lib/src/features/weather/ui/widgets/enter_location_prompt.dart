import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:weather_app/src/features/weather/bloc/bloc.dart';
import 'package:weather_app/src/features/weather/ui/pages/pages.dart';

class EnterLocationPrompt extends StatelessWidget {
  const EnterLocationPrompt({
    Key key,
    @required this.width,
  }) : super(key: key);

  final double width;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.all(25.0),
          child: Text(
            'Please Select a Location',
            style: TextStyle(
                color: Colors.white,
                fontSize: 24.0,
                fontWeight: FontWeight.w400),
          ),
        ),
        SizedBox(
          width: width * .6,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              padding: EdgeInsets.all(10.0),
              primary: Color(0xff9cbaff),
            ),
            onPressed: () async {
              final location = await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => CitySelectPage()));
              if (location != null) {
                BlocProvider.of<WeatherBloc>(context)
                    .add(WeatherRequested(city: location));
              }
            },
            child: Text('SEARCH'),
          ),
        )
      ],
    );
  }
}
